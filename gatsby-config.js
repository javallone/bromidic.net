module.exports = {
  siteMetadata: {
    title: 'Bromidic.net',
    description: 'Personal blog of Jeff Avallone.',
    siteUrl: 'https://bromidic.net',
    postsPerPage: 10
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: 'gatsby-plugin-nprogress',
      options: {
        color: 'var(--color-red)'
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'post',
        path: `${ __dirname }/src/blog`
      }
    },
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          'gatsby-remark-external-links',
          {
            resolve: 'gatsby-remark-prismjs',
            options: {
              inlineCodeMarker: '>',
              showLineNumbers: true
            }
          }
        ]
      }
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: [
            'Raleway:400,800',
            'Libre Baskerville:400,400i,700'
          ]
        }
      }
    },
    'gatsby-plugin-catch-links',
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        exclude: ['/', '/posts/*']
      }
    },
    {
      resolve: 'gatsby-plugin-feed',
      options: {
        feeds: [
          {
            output: '/rss.xml',
            query: `{
              allMarkdownRemark(
                limit: 100,
                sort: { fields: [frontmatter___date], order: DESC }
                filter: { fields: { published: { eq: true } } }
              ) {
                edges {
                  node {
                    fields {
                      slug
                    }
                    frontmatter {
                      title,
                      date
                    }
                    excerpt(pruneLength: 200)
                    html
                  }
                }
              }
            }`
          }
        ]
      }
    }
  ]
};
