const path = require('path');
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;

  if (node.internal.type === 'MarkdownRemark') {
    const relativeFilePath = createFilePath({
      node,
      getNode,
      basePath: 'src/blog/',
      trailingSlash: true
    });

    createNodeField({
      node,
      name: 'slug',
      value: `/post${ relativeFilePath }`
    });

    createNodeField({
      node,
      name: 'published',
      value: !!node.frontmatter.date &&
        new Date(node.frontmatter.date) < Date.now()
    });
  }
};

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const publishedOnly = process.env.NODE_ENV === 'production';
  const postPage = path.resolve('src/templates/Post/index.js');
  const postListPage = path.resolve('src/templates/PostList/index.js');

  const result = await graphql(`
    {
      site {
        siteMetadata {
          postsPerPage
        }
      }
      posts: allMarkdownRemark(
        filter: {
          fields: { published: { in: [true, ${ publishedOnly }] } }
        }
      ) {
        edges {
          node {
            fields {
              slug,
              published
            }
          }
        }
      }
    }
  `);

  if (result.errors) {
    console.log(result.errors);
    throw result.errors;
  }

  const posts = result.data.posts.edges;

  posts.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      component: postPage,
      context: {
        slug: node.fields.slug
      }
    });
  });

  const postsPerPage = result.data.site.siteMetadata.postsPerPage;
  const numPages = Math.ceil(posts.length / postsPerPage);
  const pagePaths = Array.from(Array(numPages),
    (_, pageNum) => `/posts/${ pageNum + 1 }`);
  const listPages = pagePaths.map((path, pageNum) => ({
    path,
    component: postListPage,
    context: {
      slug: pageNum === 0 ? '/' : path,
      publishedOnly,
      skip: pageNum * postsPerPage,
      limit: postsPerPage,
      nextPage: pagePaths[pageNum + 1],
      prevPage: pagePaths[pageNum - 1],
      numPages,
      pageNum
    }
  }));

  listPages.forEach(page => createPage(page));
  createPage({ ...listPages[0], path: '/' });
};

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, 'src'), 'node_modules']
    }
  });
};
