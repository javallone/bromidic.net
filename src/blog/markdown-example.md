---
title: "Markdown Example"
---
# Header 1

## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6

---

# Paragraph

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat augue
non risus pellentesque, ac porta nisl luctus. Aliquam aliquet, neque eu egestas
fringilla, augue neque suscipit quam, eget auctor purus nisl non nulla. Ut
fringilla maximus accumsan. Maecenas nec porta nulla, ut convallis nulla.
Vivamus eu aliquam nunc. Ut non magna sit amet felis congue efficitur. Duis nisi
dolor, hendrerit ac tempor at, sollicitudin ut nisl. Quisque tincidunt, ligula
vel pulvinar molestie, magna tortor aliquam purus, placerat interdum lectus
ligula facilisis velit. Donec luctus, tellus sagittis consequat sodales, ligula
quam pellentesque libero, nec faucibus orci eros id ipsum. Nulla imperdiet
aliquam arcu in scelerisque. Aliquam sodales diam nec risus mattis, vitae
lobortis lorem vehicula. Ut mauris erat, maximus in sem ac, mollis pretium quam.
Fusce vestibulum sollicitudin sapien eu porttitor. Donec et vehicula elit, vitae
pulvinar arcu. Sed nisl purus, fringilla at magna et, fringilla vulputate
turpis. Donec et neque aliquet, mattis tellus quis, aliquet lorem.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat augue
non risus pellentesque, ac porta nisl luctus. Aliquam aliquet, neque eu egestas
fringilla, augue neque suscipit quam, eget auctor purus nisl non nulla. Ut
fringilla maximus accumsan. Maecenas nec porta nulla, ut convallis nulla.
Vivamus eu aliquam nunc. Ut non magna sit amet felis congue efficitur. Duis nisi
dolor, hendrerit ac tempor at, sollicitudin ut nisl. Quisque tincidunt, ligula
vel pulvinar molestie, magna tortor aliquam purus, placerat interdum lectus
ligula facilisis velit. Donec luctus, tellus sagittis consequat sodales, ligula
quam pellentesque libero, nec faucibus orci eros id ipsum. Nulla imperdiet
aliquam arcu in scelerisque. Aliquam sodales diam nec risus mattis, vitae
lobortis lorem vehicula. Ut mauris erat, maximus in sem ac, mollis pretium quam.
Fusce vestibulum sollicitudin sapien eu porttitor. Donec et vehicula elit, vitae
pulvinar arcu. Sed nisl purus, fringilla at magna et, fringilla vulputate
turpis. Donec et neque aliquet, mattis tellus quis, aliquet lorem.

# Footnotes

Footnotes can be inline[^like this.], or they can reference an explicit
footnote[^example].

[^example]: This is an explicit footnote

# Styles

* Plain text
* _Italic text_
* **Bold text**
* Links: [On-site](/), [Off-site](http://www.google.com)
* Blockquotes
> Work in lists

# Lists

## Unordered

* Item 1
* Item 2
* Item 3
    * Subitem

## Ordered

1. Item 1
1. Item 2
1. Item 3
    1. Subitem

## Task List

- [ ] Item 1
- [x] Item 2
- [ ] Item 3
    - [x] Subitem

# Code

Inline code `javascript>alert('With language spec.');` and code blocks:

```javascript{2,3}
function exampleCode() {
  // Contrived code example
  alert(['Foo', 'Bar'].join(' '));
}
```

# Tables

| Header | Another Header |
| ------ | -------------- |
| Row 1  | Some content   |
| Row 2  | More content   |
