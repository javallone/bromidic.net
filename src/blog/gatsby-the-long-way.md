---
title: "Gatsby: The Long Way"
date: "2018-12-29"
---
I'm bad at blogging.

Not that I'm bad at writing (maybe I am), I'm just inconsistent at posting to a
blog. For example, this is the inaugural post of a relaunch of this site...the
previous post was the inaugural post of a relaunch and it was posted almost 8
years ago.

My problem is generally coming up with something to write about, so I'm going to
start this off with an obvious topic: Setting up this exact blog.

# The Right Way

I read the instructions, then opted not to follow them. I'd suggest anyone
wanting to get started should go to [Gatsby's site](https://www.gatsbyjs.org)
and follow the Quick Start or the tutorials. Use a starter (there are tons of
them and they're very nice).

That's the **right way** to get started with Gatsby.

# The Less-Right Way

You don't _actually_ need a starter to use Gatsby, it's just much easier to go
that route. The upside of going without a starter is that you get to make all
the decisions and you (hopefully) know how everything is working.

First off, setup a node project. Get a `package.json` file setup however you
please. Also, you'll probably want a `.gitignore` file containing the following
at a minimum:

```
# Gatsby files
.cache/
public/
```

## Minimal Gatsby Setup

Now, install the packages that would have been installed by Gatsby's tooling:
`gatsby`, `prop-types`, `react`, and `react-dom`. It will probably be helpful to
setup some scripts in your `package.json` file to run `gatsby build` and `gatsby
develop`.

You'll also need a minimal `gatsby-config.js` file:

```javascript
module.exports = {
  siteMetadata: {
    title: 'Site Title'
  },
  plugins: []
};
```

Finally, you'll need something to render, so setup a minimal page at
`src/pages/index.js`:

```javascript
import React from 'react';

const IndexPage = () => <div>Hello World</div>;

export default IndexPage;
```

Now you can start a development server using `gatsby develop` (or the script you
setup in `package.json`) and view your site at http://localhost:8000/.

## Start Adding Plugins

The plugins for Gatsby generally have very good documentation for setting them
up and using them, so I'm going to avoid going into too much detail on most of
them.

I'm starting out with the following plugins, which seem to be pretty common for
the blogging-related starters:

* [gatsby-plugin-catch-links][7]: Handle links in the page without having to use
  Gatsby's `<Link>` component.
* [gatsby-plugin-feed][9]: Generate an RSS feed
* [gatsby-plugin-nprogress][2]: Not strictly necessary. It adds a loading
  indicator at the top of the page if things are taking too long.
* [gatsby-plugin-react-helmet][1]: Setup tags in the document's `<head>`
* [gatsby-plugin-sitemap][8]: Generate a `sitemap.xml` for your site. I've set
  this up to exclude pages that are just a list of posts.
* [gatsby-plugin-web-font-loader][5]: Load web fonts from various sources. I was
  originally using [gatsby-plugin-typography][6], but I found myself fighting
  with it a little too much and decided to handle styles on my own.
* [gatsby-source-filesystem][3]: Create pages from files in a directory
* [gatsby-transformer-remark][4]: Generate pages from markdown files
  * [gatsby-remark-prismjs][10]: Plugin for gatsby-transformer-remark to
    integrate PrismJS for syntax highlighting.

Getting gatsby-source-filesystem and gatsby-transformer-remark to work for a
blog does require some setup. Specifically, a `gatsby-node.js` file has to be
created to update the GraphQL data used to render the site. There's enough
involved in that to warrant a separate blog post though. Until I write that up,
take a look at the [bromidic.net source](https://gitlab.com/javallone/bromidic.net),
specifically the `gatsby-node.js` file and `src/templates/Post/index.js`

There are some other plugins that are commonly used that I'm not using right
now, such as plugins to load images. See
[Gatsby's Plugin Library](https://www.gatsbyjs.org/plugins/) for what you might
need.

[1]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-react-helmet
[2]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-nprogress
[3]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-source-filesystem
[4]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-transformer-remark
[5]: https://github.com/escaladesports/gatsby-plugin-web-font-loader
[6]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-typography
[7]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-catch-links
[8]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-sitemap
[9]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-plugin-feed
[10]: https://github.com/gatsbyjs/gatsby/tree/master/packages/gatsby-remark-prismjs
